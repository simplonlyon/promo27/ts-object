# Typescript X Objects

Projet typescript avec des objets et des interfaces (et du DOM)

## How To Use
1. Cloner le projet
2. Lancer le `npm i`
3. Lancer le serveur `npm run dev`
4. Accéder au projet sur http://localhost:5173

## Exercices
### Premiers objets et interface ([ts](src/exo-first.ts))
1. Créer un fichier exo-first html et ts
2. Dans le fichier TS créer une interface Todo avec un label en string et un done en boolean
3. Créer une variable todoList typée en tableau de Todo et mettre 1 ou 2 objets Todo à l'intérieur
4. En dessous de la variable faire un push d'un nouveau Todo et ensuite faire un for..of sur la todoList pour parcourir les todos
5. Dans la boucle, pour chaque Todo on affiche dans la console le label du Todo ainsi que ☒ si la todo est done ou ☐ si elle ne l'est pas
### Exo Chien ([ts](src/exo-dog.ts))
#### I. Le type, le tableau et l'ajout
1. Créer un fichier exo-dog.html et exo-dog.ts
2. Dans le fichier entities ou autre, créer et exporter une interface Dog qui aura comme propriétés : un id en number optionnel, un name en string, un breed en string et birthdate en Date
3. Dans le fichier exo-dog.ts, créer une variable qui contiendra un tableau de Dog
4. Créer une fonction addDog qui va attendre un paramètre de type Dog et dont le travail sera de rajouter le chien en question dans le tableau, mais surtout de lui assigner un id unique (plusieurs manière, l'une d'elle serait d'avoir une variable currentId à l'extérieur de la fonction qu'on incrémente à chaque addDog) [pour vous aider, vous pouvez commencer par faire que la fonction assigne 1 comme id à tous les chiens donnés en paramètre et dans un second temps faire que l'id soit unique]
5. Pour tester si ça marche, on peut appeler la fonction addDog comme ceci : `addDog({name:'Fido', breed:'Corgi', birthdate: new Date()})` puis faire un console.log du tableau pour voir si le chien a bien été ajouté avec un id