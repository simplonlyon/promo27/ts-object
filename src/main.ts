/**
 * Une interface est un outil qui n'existe que dans typescript et pas dans
 * Javascript qui va permettre de définir des types, des structures d'objets.
 * Ici on dit que si on type une variable ou un tableau en Person, alors sa valeur
 * devra forcément avoir une propriété name en string, une propriété firstName en string
 * et une propriété age en number.
 */
interface Person {
  name:string;
  firstName:string;
  age: number;
}

/**
 * Ici notre variable person1 contient un objet, c'est à dire un ensemble de "propriétés"
 * (les propriétés sont un peu des sous variables) regroupées ensemble. L'idée est d'organiser
 * notre code en déclarant plusieurs valeurs qui forme un tout logique. Ici on a 3
 * "variables"/propriétés, name, firstName et age, qui ensembles représentent une personne.
 * On a typée la variable avec l'interface Person ce qui fait que la variable en question
 * ne pourra contenir que des objets avec exactement la même structure que celle définie
 * dans l'interface.
 */
let person1:Person = {
  name: 'Johnson',
  firstName: 'Johnny',
  age: 54
}
/**
 * On a ici une seconde personne avec ses propres valeurs mais qui partage la structure
 * du premier objet du fait qu'ils possèdent le même typage Person
 */
let person2:Person = {
  name: 'Bobson',
  firstName: 'Bobby',
  age: 34
}
//On peut accèder à une propriété en mettant le nom de la variable suivi du nom de la propriété voulue
console.log(person1.firstName);
//Et on peut de la même manière modifier la valeur d'une propriété
person2.age = 54;

//Ici on fait un tableau qui ne pourra contenir que des objets de type Person
const persons:Person[] = [
  {name: 'test', firstName: '', age: 10}
];
//On s'en sert comme d'un tableau classique, mais chaque valeur contient un objet
console.log(persons[0].name);