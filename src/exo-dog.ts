import { Dog } from "./entities";

let listDog:Dog[] = [];
let currentId = 1;

const form = document.querySelector('form');
const inputName = document.querySelector<HTMLInputElement>('#name');
const inputBreed = document.querySelector<HTMLInputElement>('#breed');
const inputBirthdate = document.querySelector<HTMLInputElement>('#birthdate');
const section = document.querySelector('section');

/**
 * Dans le submit du formulaire, on récupère les valeurs de chacun des inputs et on
 * assigne ces valeurs à un objet de type Dog qu'on donne à notre fonction addDog
 * qui se chargera de push le dog dans le tableau (et de lui ajouter un id unique)
 */
form.addEventListener('submit', (event) =>{
    event.preventDefault();
    // console.log(inputName.value, inputBreed.value, inputBirthdate.valueAsDate);
    
    addDog({
        name: inputName.value,
        breed: inputBreed.value,
        birthdate: inputBirthdate.valueAsDate
    });
    section.innerHTML = '';
    section.append(displayList());
    
})

addDog({
    name:'Fido', 
    breed: 'Corgi', 
    birthdate: new Date('2022-01-03')
});
addDog({
    name:'Norbert', 
    breed: 'Dalmatian', 
    birthdate: new Date('2023-02-23')
});

console.log(listDog);


/**
 * Fonction qui ajoute un chien dans le tableau listDog et lui assigne un id unique
 * @param dog Le chien à ajouter dans le tableau
 */
function addDog(dog:Dog) {
    dog.id = currentId;
    listDog.push(dog);
    currentId++;
}

/**
 * Fonction dont l'objectif est de prendre un objet Dog passé en argument et de créer
 * pour cet objet Dog un élément HTML (via des createElement, mais on pourrait aussi via
 * du innerHTML en vrai) puis de renvoyer l'élément en question. 
 * /!\ Attention cette fonction n'append pas l'élément où que ce soit, elle ne fait que le renvoyer
 * il faudra donc append le résultat de la fonction à l'extérieur pour l'append où on le souhaite
 * @param dog Le chien pour lequel on veut créer un élément HTML
 * @returns L'élément HTML qui contiendra les informations de notre chien (ici une card bootstrap)
 */
function dogToHTML(dog:Dog):HTMLElement {
    let divCard = document.createElement('div');
    divCard.classList.add('card');

    let cardBody = document.createElement('div');
    cardBody.classList.add('card-body');
    divCard.append(cardBody);

    let cardTitle = document.createElement('h3');
    cardTitle.classList.add('card-title');
    cardTitle.textContent = dog.name;
    cardBody.append(cardTitle);

    let cardPara = document.createElement('p');
    cardPara.classList.add('card-text');
    cardPara.textContent = dog.breed +' '+dog.birthdate.toLocaleDateString();
    cardBody.append(cardPara);


    return divCard;
}

/**
 * Même fonction que dogToHTML mais en version simplifiée, avec juste un seul élément
 * HTML plutôt qu'une card bootstrap
 */
export function simpleDogToHTML(dog:Dog):HTMLElement {
    let pDog = document.createElement('div');
    pDog.textContent = dog.name;


    return pDog;
}

/**
 * Fonction qui boucle sur les chiens et qui appelera la fonction dogToHTML pour chaque chien
 * @returns L'élément HTML contenant tous les chiens qu'il faudra append là où on souhaite
 */
function displayList(): HTMLElement {
    const row = document.createElement('div');
    row.classList.add('row', 'row-cols-1', 'row-cols-md-3', 'g-3');
    for(const item of listDog) {
        const col = document.createElement('div');
        col.classList.add('col');
        col.append(dogToHTML(item));
        row.append(col);
    }
    return row;
}

section.append(displayList());