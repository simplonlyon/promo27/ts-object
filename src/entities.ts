/**
 * Dans les "vrais" projets front end, on aura l'habitude de scinder notre code en
 * plusieurs fichiers afin de mieux s'organiser. Le mécanisme permettant de faire ça en
 * JS consiste à rajouter un export devant les variables/fonctions/interfaces qu'on souhaite
 * pouvoir utiliser dans d'autres fichiers. Dans les fichiers en question, il faudra
 * utiliser un import pour indiquer que certaines valeurs viennent d'autres fichiers
 */
export interface Todo {
    label:string;
    done:boolean;
}

export interface Dog {
    id?:number;
    breed:string;
    name:string;
    birthdate:Date;
}

export interface Recipe {
    title:string;
    serves:number;
    ingredients:Ingredient[];
}

export interface Ingredient {
    label: string;
    quantity: number;
    unit: string;
}