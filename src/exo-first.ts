/**
 * Ici, on importe l'interface Todo qui est déclarée à l'intérieur du fichier entities.ts
 * La plupart du temps il n'est pas nécessaire d'écrire nous même l'import, on demande
 * plutôt à VSCode de le faire soit en autocomplétant avec ctrl + espace, soit en
 * cliquant sur l'ampoule bleue (ou avec ctrl+shift+; )
 */
import { Todo } from "./entities";



let todoList:Todo[] = [
    {label: 'Buy milk', done: false},
    {label: 'Do my taxes', done: true},
    {label: 'Learn typescript', done: false},
];

todoList.push({label: 'Do stuff', done: true});

for (const todo of todoList) {
    if(todo.done) {
        console.log('☒ '+todo.label);
    }else {
        console.log('☐ '+todo.label);

    }
}
