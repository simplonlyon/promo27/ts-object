import { Recipe } from './entities';
import data from './recipes.json';

let recipes:Recipe[] = data;
const para = document.querySelector<HTMLElement>('p');
const h2 = document.querySelector<HTMLElement>('h2');
const ingredientsUl = document.querySelector<HTMLElement>('#ingredients');

const ul = document.querySelector<HTMLElement>('.dropdown-menu');
for (const item of recipes) {
    const li = document.createElement('li');
    const a = document.createElement('a');
    li.append(a);
    a.textContent = item.title;
    a.href = '#';
    a.classList.add('dropdown-item');
    ul.append(li);

    a.addEventListener('click', () => {
        h2.textContent = item.title;
        para.textContent = 'For '+item.serves+' people';

        ingredientsUl.innerHTML = '';
        for (const ingredient of item.ingredients) {
            
            const liIngr = document.createElement('li');
            //Les deux lignes suivantes font la même chose
            liIngr.textContent = `${ingredient.label} ${ingredient.quantity} ${ingredient.unit}`;
            liIngr.textContent = ingredient.label+' '+ingredient.quantity+' '+ingredient.unit;
            ingredientsUl.append(liIngr);
        }
    });
}